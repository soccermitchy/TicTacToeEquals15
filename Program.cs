﻿using System;
using System.Linq;

namespace ttt15
{
    class Program
    {
        static void Main(string[] args)
        {
            // I wrote a whole explaination for it while I was reinstalling my laptop. Windows decided not to save 
            // it and I'm on linux now to rewrite it, but I'm not gonna bother to expalin it in-depth again. Hopefully
            // the code is enough documetation. (I explained what I did where it isn't). I basically took a brute force
            // approach to this, but I do eliminate many values very quickly in a single line of code (the one with
            // combinationString.Any();)
            var totalValidTries = 0;
            var combination = 123456789; // I think this is actually the lowest possible... we shall see. It still needs
                                         // to search, but hopefully this cuts down on the time for it. 
            while (true)
            {
                combination++;

                var combinationString = combination.ToString().PadLeft(9, '0');
                var combinationIsInvalid = combinationString.Any(c => c == '0' || combinationString.Count(c2 => c2 == c)>1);
                if (totalValidTries == 0 && combination % 1000000 == 0) {
                    Console.WriteLine("Finding start position, currently at " + combination);
                }
                if (combinationIsInvalid) continue;
                totalValidTries++;
                if (totalValidTries % 10000 == 0) {
                    Console.WriteLine("Current combination: " + combination + " -- Total valid tries: " + totalValidTries);
                }

                // yes, I am subtracting two characters. This works because strings are character arrays, and due to the order of elements in the ASCII table,
                // with '0' being before '1', I can just take a number (ie. 1, ASCII value 49) and subtract '0' (the character, ascii value 48), which leaves
                // me with '1' - '0' = 49 - 48 = 1.
                var board = combinationString.Select(a => a - '0').ToArray();

                // Mostly just a reference for myself.
                // 0 1 2
                // 3 4 5
                // 6 7 8
                if (!(board[0] + board[1] + board[2] == 15)) continue;
                if (!(board[3] + board[4] + board[5] == 15)) continue;
                if (!(board[6] + board[7] + board[8] == 15)) continue;
                if (!(board[0] + board[3] + board[6] == 15)) continue;
                if (!(board[1] + board[4] + board[7] == 15)) continue;
                if (!(board[2] + board[5] + board[8] == 15)) continue;
                if (!(board[0] + board[4] + board[8] == 15)) continue;
                if (!(board[6] + board[4] + board[2] == 15)) continue;
                break; // we got the combination, so we can break out of the loop.
            }
            Console.WriteLine("Combination: " + combination);
            Console.WriteLine("Number of (valid) Tries: " + totalValidTries);
        }
    }
}
